<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css">
</head>

<?php
    $categories = ["humanitarian", "technology", "education", "environmental", "animals", "elderly"];
    $success = "none";
    if (isset($_POST["title"]) && $_POST["title"] != "" && isset($_POST["zipCode"]) && isset($_POST["date"])) {
        $bitCat = 0;
        for ($i = 0; $i < count($categories); $i++) {
            if (isset($_POST[$categories[$i]])) {
                $bitCat += pow(2, $i);
            }
        }
        $request = sprintf(
            "INSERT INTO Opportunities (zipCode, category, date, keywords, title) VALUES ('%s', %d, '%s', '%s', '%s')"
            , $_POST["zipCode"], $bitCat, $_POST["date"], str_replace(',', '|', $_POST["keywords"]), $_POST["title"]);
        $url = "http://teamvypr.org/cs_server/server.php";
        $params = array('request' => $request);
        
    // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($params)
            )
        );
        $context  = stream_context_create($options);
        $success = file_get_contents($url, false, $context);
    }
?>

<script> 
    function submitEvent() {
        let categories = <?php echo "[\"".implode("\", \"", $categories)."\"]"; ?>;
        var missing = [];
        if (document.getElementsByName(`title`)[0].value == "") {
            missing.push("title");
        }
        var checkedCategory = false;
        for (let i = 0; i < categories.length; i++) {
            if (document.getElementsByName(categories[i])[0].checked) {
                checkedCategory = true;
            }
        }
        if (!checkedCategory) {
            missing.push("category");
        }
        zipRegex = /^[0-9]{5}$/
        if (!zipRegex.test(document.getElementsByName(`zipCode`)[0].value)) {
            missing.push("zipCode");
        }
        if (document.getElementsByName("date")[0].value == false) {
            missing.push("date");
        }
        if (missing.length != 0) {
            document.getElementById("error").innerHTML = "Please enter valid: " + missing.join(", ");
            document.getElementById("error").setAttribute("class", "error");
        }
        else {
            document.forms["eventForm"].submit();
            /*document.getElementById("error").innerHTML = "Successfully submitted!";
            document.getElementById("error").setAttribute("class", "success");*/
        }
    }
</script>

<body>
    <div class="header">
        <h1>Community Service Council</h1>
    </div>
    <div class="topnav">
        <a href=".">Home</a>
        <a href="search.php">Search</a>
        <a href="add.php">Add</a>
    </div>
    <form action="add.php" class="parameters" method = "post" id = "eventForm">
        <h3>Add Event</h3>
        Title: <input type="text" name = "title"><br>
        Category (at least 1):<br>
        <input type="checkbox" name = "humanitarian">Humanitarian<br>
        <input type="checkbox" name = "technology">Technology<br>
        <input type="checkbox" name = "education">Education<br>
        <input type="checkbox" name = "environmental">Environmental<br>
        <input type="checkbox" name = "animals">Animals<br>
        <input type="checkbox" name = "elderly">Elderly<br>
        ZipCode: <input type="text" name = "zipCode"><br>
        Date: <input type="date" name = "date"><br>
        Optional Keywords (separate with commas): <input type="text" name = "keywords"><br>
    </form>
    <button onclick="submitEvent()">Submit</button>
    <p id="error" class="error"></p>
    <script>
        var success = <?php echo "\"" . $success . "\""; ?>;
        if (success == "none") {
        }
        else if (success == "duplicate") {
            document.getElementById("error").innerHTML = "Cannot add duplicate event.";
            document.getElementById("error").setAttribute("class", "error");
        }
        else if (success == "failure") {
            document.getElementById("error").innerHTML = "Sorry, something went wrong adding your event.";
            document.getElementById("error").setAttribute("class", "error");
        }
        else {
            document.getElementById("error").innerHTML = "Successfully added event!";
            document.getElementById("error").setAttribute("class", "success");
        }
    </script>
</body>
</html>