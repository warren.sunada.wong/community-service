
<?php

    $ch = curl_init();
    $request2 = 'SELECT * FROM Opportunities WHERE ';
    $categories = ["humanitarian", "technology", "education", "environmental", "animals", "elderly"];
    
    $success = "none";
    if (isset($_POST["zipCode"]) || isset($_POST["review"])) {
        $count = 0;
        $reqs = "";
        $catReq = "";
        for ($i = 0; $i < count($categories); $i++) {
            if (isset($_POST[$categories[$i]])) {
                if ($count != 0) {
                    $catReq .= " OR";
                }
                $catReq .= " category & ". pow (2, $i) . " > 0";
                $count++;
            }
        }
        if ($catReq == "") {
            $catReq = "1";
        }
        $reqs .= "(".$catReq.")";
        $dateReq = "";
        if (isset($_POST["startDate"]) && $_POST["startDate"] != null) {
            $dateReq .= ("date > \"" . $_POST["startDate"] . "\"");
        }
        if (isset($_POST["endDate"]) && $_POST["endDate"] != null) {
            if (isset($_POST["startDate"]) && $_POST["startDate"] != null) {
                $dateReq .= " AND ";
            }
            $dateReq .= "date < \"" .$_POST["endDate"] . "\"";
        }
        if ($dateReq == "") {
            $dateReq = "1";
        }
        $reqs .= " AND (".$dateReq.")";

        $zipReq = "";
        if (isset($_POST["zipCode"]) && $_POST["zipCode"]) {
            $zipReq = "zipCode = \"" . $_POST["zipCode"] . "\"";
        }
        else {
            $zipReq = "1";
        }
        $reqs .= " AND (".$zipReq.")";

        $keyReq = "";
        if (isset($_POST["keywords"]) && $_POST["keywords"]) {
            $keys = explode(",",$_POST["keywords"]);
            for ($i = 0; $i < count($keys); $i++) {
                if ($i != 0) {
                    $keyReq .= "AND ";
                }
                $keyReq .= "keywords like \"%" . $keys[$i] . "%\"";
            }
        }
        else {
            $keyReq = "1";
        }
        $reqs .= " AND (".$keyReq.")";

        $request2 .= $reqs;
        //echo $request2;
        $url = "http://teamvypr.org/cs_server/server.php";
        $params = array('request' => $request2);
        
    // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($params)
            )
        );
        $context  = stream_context_create($options);
        if (isset($_POST["opportunityID"]) && isset($_POST["review"])) {
            $request = sprintf("INSERT INTO Reviews(opportunityID, text) VALUES (%d,\"%s\")", $_POST["opportunityID"], $_POST["review"]);
            //echo $request;
            $params = array('request' => $request);

        // use key 'http' even if you send the request to https://...
            $options['http']['content'] = http_build_query($params);
            $context  = stream_context_create($options);
            $success = file_get_contents($url, false, $context);
            
        }
        $params = array('request' => $request2);
        $options['http']['content'] = http_build_query($params);
        $context  = stream_context_create($options);
        $data = file_get_contents($url, false, $context);
        // echo $data;
        $reviewsReq = "SELECT Reviews.reviewID, Opportunities.opportunityID, Reviews.text FROM Reviews, Opportunities WHERE " . $reqs . " AND Opportunities.opportunityID=Reviews.opportunityID";
        //echo $reviewsReq . "<br>";
        $params = array('request' => $reviewsReq);
        $options['http']['content'] = http_build_query($params);
        $context  = stream_context_create($options);
        $reviews = file_get_contents($url, false, $context);
    }
    else {
        $data = "[]";
        $reviews = "[]";
    }
    //echo $reviews; 
    
?>
<script>
    var eventsFound = <?php echo $data; ?>;
    var categories = <?php echo "[\"".implode("\", \"", $categories)."\"]"; ?>;
    var reviews = <?php echo $reviews; ?>;
    var success = <?php echo "\"" . $success . "\""; ?>;
</script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css">
    <script src="jquery-3.4.1.min.js"></script>
</head>
<body>
    <div class="header">
        <h1>Community Service Council</h1>
    </div>
    <div class="topnav">
        <a href=".">Home</a>
        <a href="search.php">Search</a>
        <a href="add.php">Add</a>
    </div>
    
    <form action="search.php" class="parameters" method = "post">
        <h3>Search Parameters</h3>
        <input type="checkbox" name = "humanitarian">Humanitarian<br>
        <input type="checkbox" name = "technology">Technology<br>
        <input type="checkbox" name = "education">Education<br>
        <input type="checkbox" name = "environmental">Environmental<br>
        <input type="checkbox" name = "animals">Animals<br>
        <input type="checkbox" name = "elderly">Elderly<br>
        ZipCode: <input type="text" name = "zipCode"><br>
        Start Date: <input type="date" name = "startDate">
        End Date: <input type="date" name = "endDate"><br>
        Keywords (separate with commas): <input type="text" name = "keywords"><br>
        <input type="submit" value="Submit">
    </form>
    <p id="error" class="error"></p>
    <script>
        if (success == "none") {
        }
        else if (success == "duplicate") {
            document.getElementById("error").innerHTML = "Cannot add duplicate review.";
            document.getElementById("error").setAttribute("class", "error");
        }
        else if (success == "failure") {
            document.getElementById("error").innerHTML = "Sorry, something went wrong adding your review.";
            document.getElementById("error").setAttribute("class", "error");
        }
        else {
            document.getElementById("error").innerHTML = "Successfully added review!";
            document.getElementById("error").setAttribute("class", "success");
        }
    </script>
    <!--
    <p>
        <?php 
            echo "humanitarian: ";
            if( isset($_POST["humanitarian"])) {
                echo $_POST["humanitarian"]; 
            }
            else {
                echo "off";
            }
        ?>
    </p>-->
    <p id="demo"></p>
    <script> 
        function submitReview(i) {
            var review = document.getElementById(`textarea${i}`).value;
            if (review != "") {
                $("<input />").attr("type", "hidden")
                    .attr("name", "review")
                    .attr("value", review)
                    .appendTo(`#form${i}`);
                $("<input />").attr("type", "hidden")
                    .attr("name", "opportunityID")
                    .attr("value", i)
                    .appendTo(`#form${i}`);
                document.forms[`form${i}`].submit();
                
            }
            else {
                alert("Empty text field");
            }
        }

        function showMore(i) {
            document.getElementById(`event${i}`).style.display = "block";
            var button = document.getElementById(`show${i}`);
            button.innerHTML = "show less";
            button.setAttribute("onClick", `showLess(${i})`);
        }
        
        function showLess(i) {
            document.getElementById(`event${i}`).style.display = "none";
            var button = document.getElementById(`show${i}`);
            button.innerHTML = "show more";
            button.setAttribute("onClick", `showMore(${i})`);
        }

        function createEvent(odd, event) {
            let i = event["opportunityID"];
            var container = document.createElement("div");
            var title = document.createElement("h3");
            title.innerHTML = event["title"] + " ";
            var seeMore = document.createElement("button");
            seeMore.innerHTML = "show more";
            seeMore.setAttribute("id", `show${i}`);
            seeMore.setAttribute("onClick", `showMore(${i})`);
            title.append(seeMore);
            container.appendChild(title);
            document.getElementById("demo").appendChild(container);
            if (odd % 2 == 0) {
                container.setAttribute("class", "results");
            }
            else {
                container.setAttribute("class", "results2");
            }
            var x = document.createElement("div");
            x.style.display = "none";
            x.setAttribute("id", "event"+i);
            var results = "";
            results += "&nbsp Date: " + event["date"] + "<br>";
            results +=  "&nbsp Zip Code: " + event["zipCode"] + "<br>";
            results +=  "&nbsp Categories: ";
            var catString = "";
            for (let j = 0; j < categories.length; j++) {
                if ((event["category"] & Math.pow(2, j)) > 0) {
                    catString += categories[j] + ", ";
                }
            }
            catString = catString.substring(0, catString.length-2);
            results +=  catString + "<br>";
            results +=  "&nbsp Keywords: ";
            var keyString = "";
            var keys =event["keywords"].split("|");
            for (let j = 0; j < keys.length; j++) {
                keyString += keys[j];
                if (j != keys.length-1) {
                    keyString += ", ";
                }
            }
            results +=  keyString + "<br>";
            x.innerHTML = results;
            button = document.createElement("BUTTON");
            button.innerHTML = "Add Review";
            button.setAttribute("onclick", `document.getElementById("text${i}").style.display = "block"`);
            
            var textDiv = document.createElement("div");
            textDiv.setAttribute("id", `text${i}`);
            textDiv.style.display = "none";
            var revForm = document.createElement("form");
            revForm.style.display = "none";
            revForm.setAttribute("id", `form${i}`);
            revForm.setAttribute("action", "search.php");
            revForm.setAttribute("method", "post");
            textDiv.appendChild(revForm);
            var text = document.createElement("textarea");
            text.setAttribute("class", "text");
            text.setAttribute("id", `textarea${i}`);
            textDiv.appendChild(text);
            textDiv.innerHTML += "<br>";
            var cancel = document.createElement("button");
            cancel.innerHTML = "Cancel";
            cancel.setAttribute("onClick", `document.getElementById("text${i}").style.display = "none"`);
            textDiv.appendChild(cancel);
            var submit = document.createElement("button");
            submit.innerHTML = `Submit`;
            submit.setAttribute("onClick", `submitReview(${i})`);
            submit.setAttribute("id", `submitReview${i}`);
            textDiv.appendChild(submit);
            //submit.setAttribute("onClick", `submitReview(${i})`);
            
            reviewDiv = document.createElement("div");
            reviewDiv.innerHTML += "<h4>Reviews</h4>";
            for (let j = 0; j < reviews.length; j++) {
                if (reviews[j]["opportunityID"] == i) {
                    var rev = document.createElement("p");
                    rev.innerHTML = "&nbsp \"" + reviews[j]["text"] + "\"";
                    reviewDiv.appendChild(rev);
                }
            }
            
            x.appendChild(reviewDiv);
            x.appendChild(button);
            x.appendChild(textDiv);
            
            container.appendChild(x);
        }

        for (let i = 0; i < eventsFound.length; i++) {
            createEvent(i, eventsFound[i]);
        }

    </script>
</body>
</html>